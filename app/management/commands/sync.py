from django.core.management.base import BaseCommand

from app.sync.transport import RabbitMQTransport


class Command(BaseCommand):
    help = "Test sync."

    def handle(self, *args, **options):
        transport = RabbitMQTransport()
        transport.start_consuming()
