import uuid

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import PermissionsMixin
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields["password"] = make_password(extra_fields["password"])
        return self.create(**extra_fields)

    def get_by_email(self, email: str):
        return User.objects.get(email__iexact=email)

    def email_is_taken(self, email: str) -> bool:
        return self.get_queryset().filter(email__iexact=email).exists()

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")

        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self.create(email=email, password=make_password(password), **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = "email"

    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    email = models.EmailField(db_index=True, unique=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now_add=True)

    objects = UserManager()

    class Meta:
        db_table = "users"
        ordering = ("id",)

    @property
    def full_name(self):
        if self.first_name or self.last_name:
            return " ".join([self.first_name, self.last_name]).strip()
        return self.email

    @property
    def display_name(self):
        return self.full_name or self.email

    def __str__(self):
        return self.display_name


class Category(models.Model):
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    name = models.CharField(max_length=255)


class SyncMe(models.Model):
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True)
