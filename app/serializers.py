from rest_framework import serializers

from app.models import SyncMe


class SyncMeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SyncMe
        fields = '__all__'
