import json

from django.core.exceptions import FieldDoesNotExist

from app.models import SyncMe
from .sync.transport import transport
from .sync.protocol import create_model_message


def create_instance(model, **kwargs):
    model_kwargs = {}
    for key, val in kwargs.items():
        try:
            model._meta.get_field(key)
            model_kwargs[key] = val
        except FieldDoesNotExist:
            pass

    return model.objects.create(**model_kwargs)


def update_instance(instance, **kwargs):
    model_kwargs = {}
    if instance._state.adding:
        raise instance.DoesNotExist
    for field, value in kwargs.items():
        try:
            instance._meta.get_field(field)
            model_kwargs[field] = value
            setattr(instance, field, value)
        except FieldDoesNotExist:
            pass
    instance.save(update_fields=model_kwargs.keys())
    return instance


def create_sync_me_entity(data):
    entity = create_instance(SyncMe, **data)
    message = create_model_message(entity)
    transport.send(message.to_json())
    return entity
