import json

from django.db.models.base import ModelBase
from django.forms import model_to_dict
from django.utils import timezone
from django.apps import apps


class SyncMessage:
    """
    {
        "headers": {
            "type" : "model|rpc",
            "t-date": "transaction date time",
            "action": "crud|cr|cu|d|r",
            "target": "jc_clound.models.Project|jc_clound.sync.handlers.mesasurements"
        },
        "data": {
            "json of data"
        }
    }
    """
    headers = {}
    body = {}

    def __init__(self, body, headers=None):
        if not headers:
            headers = {}
        self.headers = headers
        self.body = body

    def is_model(self):
        return self.headers.get('type') == 'model'

    def is_rps(self):
        return self.headers.get('type') == 'rpc'

    def is_create(self):
        action = self.headers.get('action', '')
        return action.find('c') > -1

    @staticmethod
    def from_dict(data: dict):
        return SyncMessage(body=data.get('body'), headers=data.get('headers'))

    def __str__(self):
        return str(self.to_dict())

    def to_dict(self):
        params = {}
        for key in self.body:
            params[key] = str(self.body[key])
        return {'headers': self.headers, 'body': self.body}

    def to_json(self):
        params = {}
        for key in self.body:
            params[key] = str(self.body[key])
        return json.dumps({'headers': self.headers, 'body': params})


def create_model_message(entity: ModelBase):
    """
    {
        "headers": {
            "type" : "model|function",
            "t-date": "transaction date time",
            "action": "crud|cr|cu|d|r",
            "target": "jc_clound.models.Project|jc_clound.sync.handlers.mesasurements"
        },
        "data": {
            "json of data"
        }
    }
    :param entity:
    :return:
    """
    target = f"{entity._meta.app_label}.{entity._meta.object_name}"

    data = model_to_dict(entity)
    # TODO check all relations and convert it to dict
    if entity.pk:
        data['id'] = str(entity.pk)

    return SyncMessage(data, {
        'type': 'model',
        't-date': timezone.now(),
        'action': 'c',
        'target': target
    })


def handle_create_model_message(message):
    # mymodel = get_model('some_app', 'SomeModel')
    model = apps.get_model(*message.headers.get('target', '.').split(':'))
    from app.services import create_instance
    instance = create_instance(model, **message.body)
    return instance


def import_function(func_str):
    parts = func_str.split('.')
    if len(parts) == 0:
        return None

    func_name = parts.pop()
    module = __import__('.'.join(parts))  # Import module and submodules

    from collections import deque
    parts = deque(parts)
    parts.popleft()  # Remove fist item

    func = None
    while len(parts) > 0:
        name = parts.popleft()
        if not func:
            func = getattr(module, name)
        else:
            func = getattr(func, name)
    return getattr(func, func_name)


def handle_rpc_message(message):
    func = import_function(message.headers.get('target'))
    func(**message.body)


def handle_message(message: SyncMessage):
    if message.is_model():
        print('model message')
        if message.is_create():
            print('create message')
            handle_create_model_message(message)

    if message.is_rps():
        print('rpc message')
        handle_rpc_message(message)
