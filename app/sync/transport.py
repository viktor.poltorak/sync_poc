import json

import pika

from . import protocol


class RabbitMQTransport:
    queue_name = 'sync'

    def __init__(self):
        rmq_parameters = pika.URLParameters("amqp://guest:guest@localhost:5672")
        self.connection = pika.BlockingConnection(rmq_parameters)
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=self.queue_name, durable=True)

    def send(self, body):
        return self.channel.basic_publish(exchange='',
                                          routing_key=self.queue_name,
                                          body=body)

    def __del__(self):
        self.connection.close()

    def start_consuming(self):
        self.channel.basic_consume(on_message_callback=self.handle_message,
                                   queue=self.queue_name)
        self.channel.start_consuming()

    def handle_message(self, ch, method, properties, body):
        ch.basic_ack(delivery_tag=method.delivery_tag)
        try:
            data = json.loads(body)
        except json.decoder.JSONDecodeError:
            data = {"body": str(body)}

        message = protocol.SyncMessage.from_dict(data)
        protocol.handle_message(message)


# Common transport for project
transport = RabbitMQTransport()
