from django.urls import include, path
from rest_framework.routers import SimpleRouter

from . import views_api

router = SimpleRouter()
router.register("sync-me", views_api.SyncMeViewSet, basename="sync-me-api")

urlpatterns = [
    path("test/", views_api.TestApi.as_view()),
    path("", include(router.urls)),
]
