from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from app.models import SyncMe
from app.serializers import SyncMeSerializer

from .sync.transport import transport
from .sync import protocol
from . import services


class TestApi(APIView):
    def get(self, request):
        # Model create message
        data = {
            'headers': {
                'type': 'model',
                't-date': '2020-05-06 16:28:54.082769+00:00',
                'action': 'c',
                'target': 'app.SyncMe'
            },
            'body': {
                'name': 'asdfdf',
                'category': None,
                'id': 'e3003717-9239-4af6-9b86-683ba72e1cc2'
            },
        }
        # Remove procedure call message
        data = {
            'headers': {
                'type': 'rpc',
                't-date': '2020-05-06 16:28:54.082769+00:00',
                'target': 'app.utils.call_me'
            },
            'body': {
                'param1': '1',
                'param2': '2',
                'param3': '3',
            },
        }

        message = protocol.SyncMessage(body=data['body'], headers=data['headers'])
        # instance = protocol.handle_rpc_message(message)
        # return Response(SyncMeSerializer(instance).data)
        transport.send(message.to_json())
        return Response({'good'})


class SyncMeViewSet(ModelViewSet):
    serializer_class = SyncMeSerializer
    queryset = SyncMe.objects.all()

    def create(self, request, *args, **kwargs):
        """
        Better to replace default DRF behavior to have full control on process

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        entity = services.create_sync_me_entity(serializer.data)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
